import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Categoria } from './interfaces/categoria.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private readonly urlAPI = 'http://localhost:3000';
  private readonly endpointCategoria = 'categorias';

  constructor(private readonly httpClient: HttpClient) { }

  public obterCategorias() {
    const url = `${this.urlAPI}/${this.endpointCategoria}`;
    return this.httpClient.get<Categoria[]>(url);
  }

  public criarCategoria(categoria: Categoria) {
    const url = `${this.urlAPI}/${this.endpointCategoria}`;
    return this.httpClient.post<Categoria>(url, categoria);
  }

  public atualizarCategoria(categoria: Categoria) {
    const url = `${this.urlAPI}/${this.endpointCategoria}/${categoria.id}`;
    return this.httpClient.put<Categoria>(url, categoria);
  }

  public excluirCategoria(idCategoria: number) {
    const url = `${this.urlAPI}/${this.endpointCategoria}/${idCategoria}`;
    return this.httpClient.delete<Categoria>(url);
  }
}
