export class Categoria {
  id: number;
  nome: string;
  descricao: string;
  situacao: string;

  constructor(categoria) {
    this.id = categoria.id;
    this.nome = categoria.nome;
    this.descricao = categoria.descricao;
    this.situacao = categoria.situacao;
  }
}
