import { Component, Inject, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Categoria } from '../interfaces/categoria.model';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import {SharedService} from '../../shared/shared.service';

@Component({
  selector: 'vex-categoria-create-update',
  templateUrl: './categoria-create-update.component.html',
  styleUrls: ['./categoria-create-update.component.scss']
})
export class CategoriaCreateUpdateComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icPerson = icPerson;
  icPhone = icPhone;

  selected: string;

  situacoes: any = ['Habilitado', 'Desabilitado'];

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
              private dialogRef: MatDialogRef<CategoriaCreateUpdateComponent>,
              private fb: FormBuilder,
              private sharedService: SharedService) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Categoria;
    }

    this.form = this.fb.group({
      id: [this.defaults.id || this.sharedService.currentIdCategoria],
      nome: this.defaults.nome,
      descricao: [this.defaults.descricao || ''],
      situacao: [this.defaults.situacao || ''],
    });
  }

  save() {
    if (this.mode === 'create') {
      this.createCategoria();
    } else if (this.mode === 'update') {
      this.updateCategoria();
    }
  }

  createCategoria() {
    const categoria = this.form.value;
    this.sharedService.changeIdCategoria(categoria.id++);
    this.dialogRef.close(categoria);
  }

  updateCategoria() {
    const categoria = this.form.value;
    categoria.id = this.defaults.id;

    this.dialogRef.close(categoria);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
