import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {id} from 'date-fns/locale';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private idCategoria = new BehaviorSubject<number>(1);
  currentIdCategoria = this.idCategoria.asObservable();

  private idFormulario = new BehaviorSubject<number>(1);
  currentIdFormulario = this.idFormulario.asObservable();

  private idColeta = new BehaviorSubject<number>(1);
  currentIdColeta = this.idColeta.asObservable();

  constructor() { }

  changeIdCategoria(idCategoria: number) {
    this.idCategoria.next(idCategoria);
  }

  changeIdFormulario(idFormulario: number) {
    this.idFormulario.next(idFormulario);
  }

  changeIdColeta(idColeta: number) {
    this.idColeta.next(idColeta);
  }
}
