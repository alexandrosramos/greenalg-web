import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Coleta } from './interfaces/coleta.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { TableColumn } from '../../../../@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import { SelectionModel } from '@angular/cdk/collections';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { stagger40ms } from '../../../../@vex/animations/stagger.animation';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { aioTableLabels } from '../../../../static-data/aio-table-data';
import { ColetaCreateUpdateComponent } from './coleta-create-update/coleta-create-update.component';
import { ColetaService } from './coleta.service';


@UntilDestroy()
@Component({
  selector: 'vex-formulario',
  templateUrl: './coleta.component.html',
  styleUrls: ['./coleta.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class ColetaComponent implements OnInit, AfterViewInit {

  layoutCtrl = new FormControl('boxed');

  /**
   * Simulating a service with HTTP that returns Observables
   * You probably want to remove this and do all requests in a service with HTTP
   */
  subject$: ReplaySubject<Coleta[]> = new ReplaySubject<Coleta[]>(1);
  data$: Observable<Coleta[]> = this.subject$.asObservable();
  coletas: Coleta[];

  @Input()
  columns: TableColumn<Coleta>[] = [
    { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Formulário', property: 'formulario', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Data', property: 'data', type: 'text', visible: true },
    { label: 'Hora', property: 'hora', type: 'text', visible: true },
    { label: 'Latitude', property: 'latitude', type: 'text', visible: false },
    { label: 'Longitude', property: 'longitude', type: 'text', visible: false },
    { label: 'Responsável', property: 'responsavel', type: 'text', visible: true },
    { label: 'Ordem', property: 'ordem', type: 'text', visible: false },
    { label: 'Classe', property: 'classe', type: 'text', visible: false },
    { label: 'Espécie', property: 'especie', type: 'text', visible: false },
    { label: 'Tipo de Atividade', property: 'tipoAtividade', type: 'text', visible: true },
    { label: 'Tipo Registro', property: 'tipoRegistro', type: 'text', visible: true },
    { label: 'Fitofisionomia', property: 'fitofisionomia', type: 'text', visible: false },
    { label: 'Tratamento Veterinário', property: 'tratamentoVeterinario', type: 'text', visible: false },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Coleta> | null;
  selection = new SelectionModel<Coleta>(true, []);
  searchCtrl = new FormControl();

  labels = aioTableLabels;

  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private readonly coletaService: ColetaService
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  getData() {
    return this.coletaService.obterColetas();
  }

  ngOnInit() {

    this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      const start = page * pageSize + 1;
      const end = (page + 1) * pageSize;
      let label = '';

      if (length < pageSize && length !== 0){
        label = `${start} - ${length} de ${length}`;
      } else if (length > 0){
        label = `${start} - ${end} de ${length}`;
      }
      return label;
    };

    this.getData().subscribe(coletas => {
      this.subject$.next(coletas);
    });

    this.dataSource = new MatTableDataSource();

    this.data$.pipe(
      filter<Coleta[]>(Boolean)
    ).subscribe(coletas => {
      this.coletas = coletas;
      this.dataSource.data = coletas;
    });

    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createColeta() {
    this.dialog.open(ColetaCreateUpdateComponent).afterClosed().subscribe((coleta: Coleta) => {
      if (coleta) {
        this.coletaService.criarColeta(coleta).subscribe(
          (response) => {
            this.coletas.push(response);
            this.subject$.next(this.coletas);
          }
        );
      }
    });
  }

  updateColeta(coleta: Coleta) {
    this.dialog.open(ColetaCreateUpdateComponent, {
      data: coleta
    }).afterClosed().subscribe(updatedColeta => {
      if (updatedColeta) {
        const index = this.coletas.findIndex((existingColeta) => existingColeta.id === updatedColeta.id);
        this.coletaService.atualizarColeta(updatedColeta).subscribe(
          (response) => {
            this.coletas.splice(index, 1);
            this.coletas.push(response);
            this.subject$.next(this.coletas);
          }
        );
      }
    });
  }

  deleteColeta(coleta: Coleta) {
    this.coletaService.excluirColeta(coleta.id).subscribe(
      (response) => {
        this.coletas.splice(this.coletas
          .findIndex((existingColeta) => existingColeta.id === coleta.id), 1);
        this.selection.deselect(coleta);
        this.subject$.next(this.coletas);
      }
    );
  }

  deleteColetas(coletas: Coleta[]) {
    coletas.forEach(c => this.deleteColeta(c));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }
}
