import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Coleta } from './interfaces/coleta.model';
import { Ordem } from './interfaces/ordem.model';
import { Classe } from './interfaces/classe.model';
import { Especie } from './interfaces/especie.model';
import { TipoAtividade } from './interfaces/tipoAtividade.model';
import { TipoRegistro } from './interfaces/tipoRegistro.model';
import { Fitofisionomia } from './interfaces/fitofisionomia.model';

@Injectable({
  providedIn: 'root'
})
export class ColetaService {
  private readonly urlAPI = 'http://localhost:3000';
  private readonly endpointColeta = 'coletas';
  private readonly endpointOrdem = 'ordens';
  private readonly endpointClasse = 'classes';
  private readonly endpointEspecie = 'especies';
  private readonly endpointTipoAtividade = 'tipoAtividades';
  private readonly endpointTipoRegistro = 'tipoRegistros';
  private readonly endpointFitofisionomia = 'fitofisionomias';

  constructor(private readonly httpClient: HttpClient) { }

  public obterOrdens() {
    const url = `${this.urlAPI}/${this.endpointOrdem}`;
    return this.httpClient.get<Ordem[]>(url);
  }

  public obterClasses() {
    const url = `${this.urlAPI}/${this.endpointClasse}`;
    return this.httpClient.get<Classe[]>(url);
  }

  public obterEspecies() {
    const url = `${this.urlAPI}/${this.endpointEspecie}`;
    return this.httpClient.get<Especie[]>(url);
  }

  public obterTipoAtividades() {
    const url = `${this.urlAPI}/${this.endpointTipoAtividade}`;
    return this.httpClient.get<TipoAtividade[]>(url);
  }

  public obterTipoRegistros() {
    const url = `${this.urlAPI}/${this.endpointTipoRegistro}`;
    return this.httpClient.get<TipoRegistro[]>(url);
  }

  public obterFitofisionomias() {
    const url = `${this.urlAPI}/${this.endpointFitofisionomia}`;
    return this.httpClient.get<Fitofisionomia[]>(url);
  }

  public obterColetas() {
    const url = `${this.urlAPI}/${this.endpointColeta}`;
    return this.httpClient.get<Coleta[]>(url);
  }

  public criarColeta(coleta: Coleta) {
    const url = `${this.urlAPI}/${this.endpointColeta}`;
    return this.httpClient.post<Coleta>(url, coleta);
  }

  public atualizarColeta(coleta: Coleta) {
    const url = `${this.urlAPI}/${this.endpointColeta}/${coleta.id}`;
    return this.httpClient.put<Coleta>(url, coleta);
  }

  public excluirColeta(idColeta: number) {
    const url = `${this.urlAPI}/${this.endpointColeta}/${idColeta}`;
    return this.httpClient.delete<Coleta>(url);
  }
}
