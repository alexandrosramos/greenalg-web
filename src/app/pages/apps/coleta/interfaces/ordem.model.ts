export class Ordem {
  id: number;
  ordem: string;

  constructor(ordem) {
    this.id = ordem.id;
    this.ordem = ordem.ordem;
  }
}
