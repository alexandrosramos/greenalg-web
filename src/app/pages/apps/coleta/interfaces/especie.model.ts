export class Especie {
  id: number;
  especie: string;

  constructor(especie) {
    this.id = especie.id;
    this.especie = especie.especie;
  }
}
