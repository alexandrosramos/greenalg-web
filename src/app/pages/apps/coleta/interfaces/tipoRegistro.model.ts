export class TipoRegistro {
  id: number;
  tipoRegistro: string;

  constructor(tipoRegistro) {
    this.id = tipoRegistro.id;
    this.tipoRegistro = tipoRegistro.tipoRegistro;
  }
}
