export class Coleta {
  id: number;
  formulario: string;
  data: string;
  hora: string;
  latitude: string;
  longitude: string;
  responsavel: string;
  ordem: string;
  classe: string;
  especie: string;
  tipoAtividade: string;
  tipoRegistro: string;
  fitofisionomia: string;
  tratamentoVeterinario: string;

  constructor(coleta) {
    this.id = coleta.id;
    this.formulario = coleta.formulario;
    this.data = coleta.data;
    this.hora = coleta.hora;
    this.latitude = coleta.latitude;
    this.longitude = coleta.longitude;
    this.responsavel = coleta.responsavel;
    this.ordem = coleta.ordem;
    this.classe = coleta.classe;
    this.especie = coleta.especie;
    this.tipoAtividade = coleta.tipoAtividade;
    this.tipoRegistro = coleta.tipoRegistro;
    this.fitofisionomia = coleta.fitofisionomia;
    this.tratamentoVeterinario = coleta.tratamentoVeterinario;
  }
}
