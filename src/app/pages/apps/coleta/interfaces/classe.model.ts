export class Classe {
  id: number;
  classe: string;

  constructor(classe) {
    this.id = classe.id;
    this.classe = classe.classe;
  }
}
