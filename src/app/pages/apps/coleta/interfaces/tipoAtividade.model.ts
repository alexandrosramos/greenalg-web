export class TipoAtividade {
  id: number;
  tipoAtividade: string;

  constructor(tipoAtividade) {
    this.id = tipoAtividade.id;
    this.tipoAtividade = tipoAtividade.tipoAtividade;
  }
}
