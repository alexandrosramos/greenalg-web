export class Fitofisionomia {
  id: number;
  fitofisionomia: string;

  constructor(fitofisionomia) {
    this.id = fitofisionomia.id;
    this.fitofisionomia = fitofisionomia.fitofisionomia;
  }
}
