import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Coleta } from '../interfaces/coleta.model';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import { ColetaService } from '../coleta.service';
import {FormularioService} from '../../formulario/formulario.service';
import {SharedService} from '../../shared/shared.service';

@Component({
  selector: 'vex-coleta-create-update',
  templateUrl: './coleta-create-update.component.html',
  styleUrls: ['./coleta-create-update.component.scss']
})
export class ColetaCreateUpdateComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icPerson = icPerson;
  icPhone = icPhone;

  selectedFormulario: string;
  selectedOrdem: string;
  selectedClasse: string;
  selectedEspecie: string;
  selectedTipoAtividade: string;
  selectedTipoRegistro: string;
  selectedFitofisionomia: string;
  selectedTratamentoVeterinario: string;

  ordens: any[] = [];
  formularios: any[] = [];
  classes: any[] = [];
  especies: any[] = [];
  tipoAtividades: any[] = [];
  tipoRegistros: any[] = [];
  fitofisionomias: any[] = [];
  tratamentoVeterinarios: any = ['Sim', 'Não'];

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
              private dialogRef: MatDialogRef<ColetaCreateUpdateComponent>,
              private fb: FormBuilder,
              private coletaService: ColetaService,
              private formularioService: FormularioService,
              private sharedService: SharedService) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Coleta;
    }

    this.form = this.fb.group({
      id: [this.defaults.id || this.sharedService.currentIdColeta],
      formulario: [this.defaults.formulario || ''],
      data: [this.defaults.data || ''],
      hora: [this.defaults.hora || ''],
      latitude: [this.defaults.latitude || ''],
      longitude: [this.defaults.longitude || ''],
      responsavel: [this.defaults.responsavel || ''],
      ordem: [this.defaults.ordem || ''],
      classe: [this.defaults.classe || ''],
      especie: [this.defaults.especie || ''],
      tipoAtividade: [this.defaults.tipoAtividade || ''],
      tipoRegistro: [this.defaults.tipoRegistro || ''],
      fitofisionomia: [this.defaults.fitofisionomia || ''],
      tratamentoVeterinario: [this.defaults.tratamentoVeterinario || ''],
    });

    this.formularioService.obterFormularios()
      .subscribe(response => {
        response.map( form => this.formularios.push(form.nome));
      });

    this.coletaService.obterOrdens()
      .subscribe(response => {
        response.map( ord => this.ordens.push(ord.ordem));
      });

    this.coletaService.obterClasses()
      .subscribe(response => {
        response.map( cla => this.classes.push(cla.classe));
      });

    this.coletaService.obterEspecies()
      .subscribe(response => {
        response.map( esp => this.especies.push(esp.especie));
      });

    this.coletaService.obterTipoAtividades()
      .subscribe(response => {
        response.map( ta => this.tipoAtividades.push(ta.tipoAtividade));
      });

    this.coletaService.obterTipoRegistros()
      .subscribe(response => {
        response.map( tr => this.tipoRegistros.push(tr.tipoRegistro));
      });

    this.coletaService.obterFitofisionomias()
      .subscribe(response => {
        response.map( f => this.fitofisionomias.push(f.fitofisionomia));
      });

  }

  save() {
    if (this.mode === 'create') {
      this.createColeta();
    } else if (this.mode === 'update') {
      this.updateColeta();
    }
  }

  createColeta() {
    const coleta = this.form.value;
    this.sharedService.changeIdColeta(coleta.id++);
    this.dialogRef.close(coleta);
  }

  updateColeta() {
    const coleta = this.form.value;
    coleta.id = this.defaults.id;

    this.dialogRef.close(coleta);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
