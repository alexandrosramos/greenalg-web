import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Formulario } from '../interfaces/formulario.model';
import icMoreVert from '@iconify/icons-ic/twotone-more-vert';
import icClose from '@iconify/icons-ic/twotone-close';
import icPrint from '@iconify/icons-ic/twotone-print';
import icDownload from '@iconify/icons-ic/twotone-cloud-download';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icPerson from '@iconify/icons-ic/twotone-person';
import {CategoriaService} from '../../categoria/categoria.service';
import {SharedService} from '../../shared/shared.service';

@Component({
  selector: 'vex-formulario-create-update',
  templateUrl: './formulario-create-update.component.html',
  styleUrls: ['./formulario-create-update.component.scss']
})
export class FormularioCreateUpdateComponent implements OnInit {

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  icMoreVert = icMoreVert;
  icClose = icClose;

  icPrint = icPrint;
  icDownload = icDownload;
  icDelete = icDelete;

  icPerson = icPerson;
  icPhone = icPhone;

  selected: string;

  categorias: any[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
              private dialogRef: MatDialogRef<FormularioCreateUpdateComponent>,
              private fb: FormBuilder,
              private categoriaService: CategoriaService,
              private sharedService: SharedService) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Formulario;
    }

    this.form = this.fb.group({
      id: [this.defaults.id || this.sharedService.currentIdFormulario],
      nome: this.defaults.nome,
      descricao: [this.defaults.descricao || ''],
      categoria: [this.defaults.categoria || ''],
    });

    this.categoriaService.obterCategorias()
      .subscribe(response => {
        response.map(cat => this.categorias.push(cat.nome));
      });

  }

  save() {
    if (this.mode === 'create') {
      this.createFormulario();
    } else if (this.mode === 'update') {
      this.updateFormulario();
    }
  }

  createFormulario() {
    const formulario = this.form.value;
    this.sharedService.changeIdFormulario(formulario.id++);
    this.dialogRef.close(formulario);
  }

  updateFormulario() {
    const formulario = this.form.value;
    formulario.id = this.defaults.id;

    this.dialogRef.close(formulario);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }
}
