import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Formulario } from './interfaces/formulario.model';

@Injectable({
  providedIn: 'root'
})
export class FormularioService {
  private readonly urlAPI = 'http://localhost:3000';
  private readonly endpointFormulario = 'formularios';

  constructor(private readonly httpClient: HttpClient) { }

  public obterFormularios() {
    const url = `${this.urlAPI}/${this.endpointFormulario}`;
    return this.httpClient.get<Formulario[]>(url);
  }

  public criarFormulario(formulario: Formulario) {
    const url = `${this.urlAPI}/${this.endpointFormulario}`;
    return this.httpClient.post<Formulario>(url, formulario);
  }

  public atualizarFormulario(formulario: Formulario) {
    const url = `${this.urlAPI}/${this.endpointFormulario}/${formulario.id}`;
    return this.httpClient.put<Formulario>(url, formulario);
  }

  public excluirFormulario(idFormulario: number) {
    const url = `${this.urlAPI}/${this.endpointFormulario}/${idFormulario}`;
    return this.httpClient.delete<Formulario>(url);
  }
}
