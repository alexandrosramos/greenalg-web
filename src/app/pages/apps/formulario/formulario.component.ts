import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Observable, of, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Formulario } from './interfaces/formulario.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { TableColumn } from '../../../../@vex/interfaces/table-column.interface';
import icEdit from '@iconify/icons-ic/twotone-edit';
import icDelete from '@iconify/icons-ic/twotone-delete';
import icSearch from '@iconify/icons-ic/twotone-search';
import icAdd from '@iconify/icons-ic/twotone-add';
import icFilterList from '@iconify/icons-ic/twotone-filter-list';
import { SelectionModel } from '@angular/cdk/collections';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icFolder from '@iconify/icons-ic/twotone-folder';
import { fadeInUp400ms } from '../../../../@vex/animations/fade-in-up.animation';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions } from '@angular/material/form-field';
import { stagger40ms } from '../../../../@vex/animations/stagger.animation';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import icPhone from '@iconify/icons-ic/twotone-phone';
import icMail from '@iconify/icons-ic/twotone-mail';
import icMap from '@iconify/icons-ic/twotone-map';
import { aioTableLabels } from '../../../../static-data/aio-table-data';
import { FormularioCreateUpdateComponent } from './formulario-create-update/formulario-create-update.component';
import {FormularioService} from './formulario.service';


@UntilDestroy()
@Component({
  selector: 'vex-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class FormularioComponent implements OnInit, AfterViewInit {

  layoutCtrl = new FormControl('boxed');

  /**
   * Simulating a service with HTTP that returns Observables
   * You probably want to remove this and do all requests in a service with HTTP
   */
  subject$: ReplaySubject<Formulario[]> = new ReplaySubject<Formulario[]>(1);
  data$: Observable<Formulario[]> = this.subject$.asObservable();
  formularios: Formulario[];

  @Input()
  columns: TableColumn<Formulario>[] = [
    { label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true },
    { label: 'Nome', property: 'nome', type: 'text', visible: true, cssClasses: ['font-medium'] },
    { label: 'Descrição', property: 'descricao', type: 'text', visible: true },
    { label: 'Categoria', property: 'categoria', type: 'text', visible: true },
    { label: 'Actions', property: 'actions', type: 'button', visible: true }
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Formulario> | null;
  selection = new SelectionModel<Formulario>(true, []);
  searchCtrl = new FormControl();

  labels = aioTableLabels;

  icPhone = icPhone;
  icMail = icMail;
  icMap = icMap;
  icEdit = icEdit;
  icSearch = icSearch;
  icDelete = icDelete;
  icAdd = icAdd;
  icFilterList = icFilterList;
  icMoreHoriz = icMoreHoriz;
  icFolder = icFolder;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private dialog: MatDialog,
    private readonly formularioService: FormularioService
  ) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  getData() {
    return this.formularioService.obterFormularios();
  }

  ngOnInit() {
    this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
      const start = page * pageSize + 1;
      const end = (page + 1) * pageSize;
      let label = '';

      if (length < pageSize && length !== 0){
        label = `${start} - ${length} de ${length}`;
      } else if (length > 0){
        label = `${start} - ${end} de ${length}`;
      }
      return label;
    };

    this.getData().subscribe(formularios => {
      this.subject$.next(formularios);
    });

    this.dataSource = new MatTableDataSource();

    this.data$.pipe(
      filter<Formulario[]>(Boolean)
    ).subscribe(formularios => {
      this.formularios = formularios;
      this.dataSource.data = formularios;
    });

    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createFormulario() {
    this.dialog.open(FormularioCreateUpdateComponent).afterClosed().subscribe((formulario: Formulario) => {
      if (formulario) {
        this.formularioService.criarFormulario(formulario).subscribe(
          (response) => {
            this.formularios.push(response);
            this.subject$.next(this.formularios);
          }
        );
      }
    });
  }

  updateFormulario(formulario: Formulario) {
    this.dialog.open(FormularioCreateUpdateComponent, {
      data: formulario
    }).afterClosed().subscribe(updatedFormulario => {
      if (updatedFormulario) {
        const index = this.formularios.findIndex((existingFormulario) => existingFormulario.id === updatedFormulario.id);
        this.formularioService.atualizarFormulario(updatedFormulario).subscribe(
          (response) => {
            this.formularios.splice(index, 1);
            this.formularios.push(response);
            this.subject$.next(this.formularios);
          }
        );
      }
    });
  }

  deleteFormulario(formulario: Formulario) {
    this.formularioService.excluirFormulario(formulario.id).subscribe(
      (response) => {
        this.formularios.splice(this.formularios
          .findIndex((existingFormulario) => existingFormulario.id === formulario.id), 1);
        this.selection.deselect(formulario);
        this.subject$.next(this.formularios);
      }
    );
  }

  deleteFormularios(formularios: Formulario[]) {
    formularios.forEach(f => this.deleteFormulario(f));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }
}
