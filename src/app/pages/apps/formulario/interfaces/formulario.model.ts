export class Formulario {
  id: number;
  nome: string;
  descricao: string;
  categoria: string;

  constructor(formulario) {
    this.id = formulario.id;
    this.nome = formulario.nome;
    this.descricao = formulario.descricao;
    this.categoria = formulario.categoria;
  }
}
